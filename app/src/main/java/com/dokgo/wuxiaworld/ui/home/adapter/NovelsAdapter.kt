package com.dokgo.wuxiaworld.ui.home.adapter

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.dokgo.wuxiaworld.R
import com.dokgo.wuxiaworld.model.Novel
import com.dokgo.wuxiaworld.model.isEmpty
import com.dokgo.wuxiaworld.model.isNotEmpty
import com.dokgo.wuxiaworld.ui.utils.ScreenUtil
import com.dokgo.wuxiaworld.ui.home.HomeFragment
import com.dokgo.wuxiaworld.ui.utils.GlideApp
import com.dokgo.wuxiaworld.ui.utils.fromHtml
import kotlinx.android.synthetic.main.item_novel.view.*

private const val STUB = 1
private const val NORMAL = 2

class NovelsAdapter(
    private val context: Context,
    private val listener: HomeFragment.ItemClickListener,
    private var isStub: Boolean
) : ListAdapter<Novel, NovelsAdapter.NovelViewHolder>(NovelDC()) {

    override fun onBindViewHolder(holder: NovelViewHolder, position: Int) {
        if (!isStub) {
            val novel = getItem(position)
            bind(novel, holder.itemView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NovelViewHolder {
        val layoutId = R.layout.item_novel_small
//            if (viewType == NORMAL) R.layout.item_novel_small
//            else R.layout.progress_item_novel_small

        val view = LayoutInflater.from(parent.context)
            .inflate(layoutId, parent, false)

        return NovelViewHolder(view)
    }

    override fun getItemViewType(position: Int): Int {
        return if (isStub) STUB else NORMAL
    }

    private fun bind(novel: Novel, itemView: View) = with(itemView) {
        transitionName = novel.name
        title.text = novel.name.fromHtml()
//        holder.novelView.author.text = Html.fromHtml(novel.authorName)
//        holder.novelView.description.text = Html.fromHtml(novel.description)
        cover.let {
            GlideApp.with(context).load(novel.coverUrl).centerCrop().into(it)
        }
        setOnClickListener {
            listener.onClick(novel, it)
        }
    }

    fun addNovels(novels: List<Novel>) {
        if (isStub) {
            isStub = false
            submitList(emptyList())
        }

        submitList(novels)
    }

    fun initAdapter(mockNovels: List<Novel>) {
        if (isStub) submitList(mockNovels)
    }

    class NovelViewHolder(novelView: View) : RecyclerView.ViewHolder(novelView)

    private class NovelDC: DiffUtil.ItemCallback<Novel>() {
        override fun areItemsTheSame(oldItem: Novel, newItem: Novel): Boolean {
            return oldItem.isNotEmpty() && oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Novel, newItem: Novel): Boolean {
            return oldItem == newItem
        }
    }
}