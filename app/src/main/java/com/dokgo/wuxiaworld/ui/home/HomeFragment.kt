package com.dokgo.wuxiaworld.ui.home

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.recyclerview.widget.GridLayoutManager
import com.dokgo.wuxiaworld.R
import com.dokgo.wuxiaworld.model.Mock
import com.dokgo.wuxiaworld.model.Novel
import com.dokgo.wuxiaworld.ui.MainActivity
import com.dokgo.wuxiaworld.ui.base.BaseFragment
import com.dokgo.wuxiaworld.ui.home.adapter.NovelsAdapter
import com.dokgo.wuxiaworld.ui.utils.ItemOffsetDecoration
import com.dokgo.wuxiaworld.ui.utils.ScreenUtil
import com.dokgo.wuxiaworld.viewmodel.Status
import com.dokgo.wuxiaworld.viewmodel.ViewModelFactory
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.home_fragment.*
import javax.inject.Inject

private const val COLUMN_COUNT = 3

class HomeFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var screen: ScreenUtil

    private lateinit var viewModel: HomeViewModel

    lateinit var adapter: NovelsAdapter
    lateinit var listener: ItemClickListener

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.home_fragment, container, false)
    }

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViewModel()
        activity?.run {
            Log.e("HomeFragment", "${this is MainActivity}")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener = object : ItemClickListener {
            override fun onClick(novel: Novel, view: View) {
                val action = HomeFragmentDirections
                    .actionHomeFragmentToDetailFragment(novel.slug, novel.name, novel.coverUrl)

                val extras = FragmentNavigatorExtras(view to novel.name)

                view.findNavController().navigate(action, extras)
                Log.e("Novel", novel.slug)
            }
        }

        initRecyclerView()
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        (activity as? MainActivity)?.showBottomNav()
    }

    private fun initListeners() {
        button_reload.apply {
            setOnClickListener {
                viewModel.getNovels()
            }
        }
    }

    private fun initViewModel() {
        viewModel =
            ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java).apply {
                novels.observe(viewLifecycleOwner, Observer {
                    adapter.addNovels(it)
                    hideProgress()
                })
                chapters.observe(viewLifecycleOwner, Observer {
                    val sb = StringBuilder()
                    it.map { sb.append(it.name) }
                })
                details.observe(viewLifecycleOwner, Observer {
                    Log.e("DETAILS", "$it")
                })
                error.observe(viewLifecycleOwner, Observer {
                    handleApiError(it)
                })

                status.observe(viewLifecycleOwner, Observer {
                    it?.let {
                        when (it) {
                            Status.Loading -> showProgress()
                            else -> hideProgress()
                        }
                    }
                })

                getNovels()
            }
    }

    private fun initRecyclerView() {
        context?.apply {
            val list = MutableList(20) { index: Int -> Mock.novel(index) }

            adapter = NovelsAdapter(this, listener, isStub = true)
            recycler_view.layoutManager = GridLayoutManager(context, COLUMN_COUNT)
            recycler_view.adapter = adapter

            recycler_view.addItemDecoration(ItemOffsetDecoration(this, R.dimen.recycler_spacing))

            adapter.initAdapter(list)
        }
    }

    private fun showProgress() {
        progress_layout.showShimmer(true)
    }

    private fun hideProgress() {
        progress_layout.hideShimmer()
    }

    interface ItemClickListener {
        fun onClick(novel: Novel, view: View)
    }
}
