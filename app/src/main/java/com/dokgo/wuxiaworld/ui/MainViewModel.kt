package com.dokgo.wuxiaworld.ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.dokgo.usecases.GetChaptersUseCase
import com.dokgo.usecases.GetNovelsUseCase
import com.dokgo.wuxiaworld.model.Chapter
import com.dokgo.wuxiaworld.model.Novel
import com.dokgo.wuxiaworld.model.toModel
import com.dokgo.wuxiaworld.ui.utils.addToDisposable
import com.dokgo.wuxiaworld.viewmodel.BaseViewModel
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getNovelsUseCase: GetNovelsUseCase,
    private val getChaptersUseCase: GetChaptersUseCase
) : BaseViewModel() {

    val error by lazy { MutableLiveData<Throwable>() }
    val novels by lazy { MutableLiveData<List<Novel>>() }
    val chapters by lazy { MutableLiveData<List<Chapter>>() }

    fun getNovels() = getNovelsUseCase.execute()
        .doOnSubscribe { Log.e("MainViewModel", "doOnSubscribe") }
        .doOnError { error.postValue(it) }
        .doFinally { Log.e("MainViewModel", "doFinally") }
        .subscribe({ list ->
            Log.e("MainViewModel", "response: $list")
            novels.postValue(list.map { it.toModel() })

        }, {
            Log.e("MainViewModel", "onError: ${it.localizedMessage}")
            it.printStackTrace()
        }).addToDisposable(this) // let { addDisposable(it) }

    fun getChapters(slug: String) = getChaptersUseCase.execute(slug)
        .doOnSubscribe { Log.e("MainViewModel", "doOnSubscribe") }
        .doOnError { error.postValue(it) }
        .doFinally { Log.e("MainViewModel", "doFinally") }
        .subscribe({ list ->
            Log.e("MainViewModel", "response: $list")
            chapters.postValue(list.map { it.toModel() })

        }, {
            Log.e("MainViewModel", "onError: ${it.localizedMessage}")
            it.printStackTrace()
        }).addToDisposable(this) // let { addDisposable(it) }
}