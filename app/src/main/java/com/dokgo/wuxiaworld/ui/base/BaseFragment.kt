package com.dokgo.wuxiaworld.ui.base

import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonSyntaxException
import retrofit2.HttpException
import java.net.HttpURLConnection

abstract class BaseFragment : Fragment() {

    fun handleApiError(error: Throwable) {
        if (error is HttpException) {
            val message = when (error.code()) {
                HttpURLConnection.HTTP_NOT_FOUND -> "NOT FOUND"
                else -> error.message()
            }

            showError(message)
        } else if (error is JsonSyntaxException) {
            showError(error.localizedMessage)
        }
    }

    fun showError(message: String, duration: Int = Snackbar.LENGTH_SHORT) {
        activity?.apply {
            Snackbar.make(findViewById(android.R.id.content), message, duration).show()
        }
    }
}