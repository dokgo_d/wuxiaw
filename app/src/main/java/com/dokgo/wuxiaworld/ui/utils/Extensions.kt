package com.dokgo.wuxiaworld.ui.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.os.Build
import android.text.Html
import android.text.Spannable
import android.text.Spanned
import android.util.Log
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import androidx.core.graphics.drawable.toBitmap
import androidx.core.text.HtmlCompat
import com.dokgo.wuxiaworld.viewmodel.BaseViewModel
import io.reactivex.disposables.Disposable

fun Int.dpToPx(): Int {
    return (this * Resources.getSystem().displayMetrics.density).toInt()
}

fun Int.pxToDp(): Int {
    return (this / Resources.getSystem().displayMetrics.density).toInt()
}

fun Disposable.addToDisposable(viewModel: BaseViewModel) {
    viewModel.addDisposable(this)
}

fun String.fromHtml() = HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//        HtmlCompat.fromHtml(this, HtmlCompat.FROM_HTML_MODE_LEGACY)
//    } else {
//        Html.fromHtml(this)
//    }

fun Context.createGradientBitmap(
    drawableId: Int,
    startColorId: Int,
    endColorId: Int,
    fill: Float = 100f
): Bitmap {
    val color1 = ContextCompat.getColor(this, startColorId)
    val color2 = ContextCompat.getColor(this, endColorId)

    val drawable = ContextCompat.getDrawable(this, drawableId)

//    val source = BitmapFactory.decodeResource(this.resources, drawableId)
    val source = drawable!!.toBitmap()

    return paintGradientBitmap(source, color1, color2, fill)
}

private fun paintGradientBitmap(
    bitmap: Bitmap,
    startColorId: Int,
    endColorId: Int,
    fill: Float
): Bitmap {
    return setGradient(bitmap, startColorId, endColorId, fill)
}

private fun setGradient(src: Bitmap, color1: Int, color2: Int, fill: Float): Bitmap {
    val w = src.width.toFloat()
    val h = src.height.toFloat()

    val result = Bitmap.createBitmap(w.toInt(), h.toInt(), Bitmap.Config.ARGB_8888)
    val canvas = Canvas(result)

    canvas.drawBitmap(src, 0f, 0f, null)

    val x1 = w / 100 * fill

    Log.e("GRAD", "x1: $x1")

    val paint = Paint()
    val shader =  if (x1 == w) LinearGradient(x1, 0f, x1, 0f, color2, color1, Shader.TileMode.CLAMP)
    else LinearGradient(0f, 0f, x1, 0f, color1, color2, Shader.TileMode.CLAMP)
    paint.shader = shader
    paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
    canvas.drawRect(0f, 0f, w, h, paint)

    return result
}