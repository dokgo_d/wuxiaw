package com.dokgo.wuxiaworld.ui.home

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.dokgo.domain.entity.NovelDetails
import com.dokgo.usecases.GetChaptersUseCase
import com.dokgo.usecases.GetNovelsUseCase
import com.dokgo.wuxiaworld.model.Chapter
import com.dokgo.wuxiaworld.model.Novel
import com.dokgo.wuxiaworld.model.toModel
import com.dokgo.wuxiaworld.viewmodel.BaseViewModel
import com.dokgo.wuxiaworld.viewmodel.Status
import javax.inject.Inject

public class HomeViewModel @Inject constructor(
    private val getNovelsUseCase: GetNovelsUseCase,
    private val getChaptersUseCase: GetChaptersUseCase
) : BaseViewModel() {

    val error by lazy { MutableLiveData<Throwable>() }
    val novels by lazy { MutableLiveData<List<Novel>>() }
    val chapters by lazy { MutableLiveData<List<Chapter>>() }
    val details by lazy { MutableLiveData<NovelDetails>() }

    fun getNovels() = getNovelsUseCase.execute()
        .doOnSubscribe { status.postValue(Status.Loading) }
        .doOnError { error.postValue(it) }
        .doFinally { status.postValue(Status.Loaded) }
        .subscribe({ list ->
            Log.e("HomeViewModel", "response: $list")
            novels.postValue(list.map { it.toModel() })

        }, {
            Log.e("HomeViewModel", "onError: ${it.localizedMessage}")
            it.printStackTrace()
        }).let { addDisposable(it) }

    fun getChapters(slug: String) = getChaptersUseCase.execute(slug)
        .doOnSubscribe { Log.e("HomeViewModel", "doOnSubscribe") }
        .doOnError { error.postValue(it) }
        .doFinally { Log.e("HomeViewModel", "doFinally") }
        .subscribe({ list ->
            Log.e("HomeViewModel", "response: $list")
            chapters.postValue(list.map { it.toModel() })

        }, {
            Log.e("HomeViewModel", "onError: ${it.localizedMessage}")
            it.printStackTrace()
        }).let { addDisposable(it) }

//    fun getDetails(slug: String) = getNovelDetailsUseCase.execute(slug)
//        .doOnSubscribe { Log.e("HomeViewModel", "doOnSubscribe") }
//        .doOnError { error.postValue(it) }
//        .doFinally { Log.e("HomeViewModel", "doFinally") }
//        .subscribe({
//            Log.e("HomeViewModel", "response: $")
//            details.postValue( it )
//        }, {
//            Log.e("HomeViewModel", "onError: ${it.localizedMessage}")
//            it.printStackTrace()
//        }).let { addDisposable(it) }
}