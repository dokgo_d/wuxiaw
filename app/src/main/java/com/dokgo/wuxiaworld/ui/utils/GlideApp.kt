package com.dokgo.wuxiaworld.ui.utils

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class MyAppGlideApp: AppGlideModule() {
}