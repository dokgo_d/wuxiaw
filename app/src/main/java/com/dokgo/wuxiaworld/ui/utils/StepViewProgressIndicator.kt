package com.dokgo.wuxiaworld.ui.utils

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.annotation.Keep
import com.dokgo.wuxiaworld.R

class StepViewProgressIndicator @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var progress = 1f
    private var progressMax = 5f
    private var strokeWidth: Float = 2.dpToPx().toFloat()
    private val rectDraw = RectF()
    private val paintBackground: Paint
    private val paintForeground: Paint
    private var animator: ObjectAnimator? = null
    private var doOnAnimationEnd: (() -> Unit)? = null

    init {
        this.paintBackground = Paint(Paint.ANTI_ALIAS_FLAG)
        this.paintBackground.style = Paint.Style.STROKE
        this.paintBackground.color = Color.RED

        this.paintForeground = Paint(Paint.ANTI_ALIAS_FLAG)
        this.paintForeground.style = Paint.Style.STROKE
        this.paintForeground.color = Color.GREEN

        var progress = this.progress
        var progressMax = this.progressMax
        var animateOnStart = false
        var colorForeground = Color.GRAY
        var colorBackground = Color.CYAN

        val typedArray =
            context.theme.obtainStyledAttributes(attrs, R.styleable.MyProgressIndicator, 0, 0)
        try {
            this.strokeWidth =
                typedArray.getDimension(R.styleable.MyProgressIndicator_stroke_width, strokeWidth)
            progress = typedArray.getFloat(R.styleable.MyProgressIndicator_progress, progress)
            progressMax =
                typedArray.getFloat(R.styleable.MyProgressIndicator_progress_max, progressMax)
            animateOnStart = typedArray.getBoolean(
                R.styleable.MyProgressIndicator_animate_on_start,
                animateOnStart
            )
            colorBackground = typedArray.getColor(
                R.styleable.MyProgressIndicator_color_background,
                colorBackground
            )
            colorForeground = typedArray.getColor(
                R.styleable.MyProgressIndicator_color_foreground,
                colorForeground
            )
        } catch (ignore: Exception) {
        } finally {
            typedArray.recycle()
        }

        this.paintBackground.strokeWidth = strokeWidth
        this.paintForeground.strokeWidth = strokeWidth

        setProgress(progress, progressMax, animateOnStart)
        setColors(colorForeground, colorBackground)
    }

    fun setColors(colorBackground: Int, colorForeground: Int) {
        this.paintForeground.color = colorForeground
        this.paintBackground.color = colorBackground
        invalidate()
    }

    fun setProgress(rating: Float, withAnimation: Boolean) {
        setProgress(rating, progressMax, withAnimation)
    }

    fun setProgress(rating: Float, ratingMax: Float, withAnimation: Boolean) {
        this.progressMax = ratingMax
        if (withAnimation) {
            setProgressWithAnimation(rating)
        } else {
            setProgress(rating)
        }
    }

    fun doOnEnd(callback: () -> Unit) {
        doOnAnimationEnd = callback
    }

    @Keep
    fun setProgress(rating: Float) {
        this.progress = rating
        invalidate()
    }

    fun setProgressWithAnimation(rating: Float) {
        try {
            if (animator != null) {
                animator!!.cancel()
            }

            animator = ObjectAnimator.ofFloat(this, "progress", rating)
            animator!!.duration = ANIMATION_DURATION.toLong()
            animator!!.interpolator = DecelerateInterpolator()
            animator!!.start()
            animator!!.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {

                }

                override fun onAnimationEnd(animator: Animator) {
                    this@StepViewProgressIndicator.animator = null
                    doOnAnimationEnd?.invoke()
                }

                override fun onAnimationCancel(animator: Animator) {
                    this@StepViewProgressIndicator.animator = null
                }

                override fun onAnimationRepeat(animator: Animator) {

                }
            })
        } catch (ignore: Exception) {
            setProgress(rating)
        }
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        canvas.drawOval(this.rectDraw, this.paintBackground)
        canvas.drawArc(
            this.rectDraw,
            180f,
            180 * (this.progress / this.progressMax),
            false,
            this.paintForeground
        )

        canvas.drawArc(
            this.rectDraw,
            180f,
            -180 * (this.progress / this.progressMax),
            false,
            this.paintForeground
        )
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = getDefaultSize(suggestedMinimumHeight, heightMeasureSpec)
        val width = getDefaultSize(suggestedMinimumWidth, widthMeasureSpec)
        val size = Math.min(width, height)
        setMeasuredDimension(size, size)

        val strokeHalf = this.strokeWidth / 2f
        this.rectDraw.set(strokeHalf, strokeHalf, size - strokeHalf, size - strokeHalf)
    }

    companion object {
        private val ANIMATION_DURATION = 600
    }
}