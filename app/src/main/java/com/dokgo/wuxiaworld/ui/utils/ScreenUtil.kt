package com.dokgo.wuxiaworld.ui.utils

import android.content.Context
import android.graphics.Point
import android.view.WindowManager
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ScreenUtil @Inject constructor(context: Context) {
    var width: Int = 0
    var height: Int = 0

    init {
        val wm =
            context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = Point()
        wm.defaultDisplay.getSize(size)

        width = size.x
        height = size.y
    }
}
