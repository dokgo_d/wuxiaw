package com.dokgo.wuxiaworld.ui.utils

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import androidx.core.animation.doOnEnd
import com.dokgo.wuxiaworld.R
import kotlinx.android.synthetic.main.step_view_layout.view.*

class StepViewLayout(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var startColor = R.color.colorProgressBg
    private var endColor = R.color.color_cyan

    init {
//        setupAttributes(attrs)
        val view = inflate(context, R.layout.step_view_layout, null).apply {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT)
        }
        addView(view)

    }

    fun onFirstStepComplete() {
        ValueAnimator.ofFloat(0f, 100f).apply {
            duration = ANIMATION_DURATION.toLong() * 3
            addUpdateListener { anim ->
                val value = anim.animatedValue as Float
                progress_icon_1.setImageBitmap(
                    context.createGradientBitmap(
                        R.drawable.ic_info,
                        endColor,
                        startColor,
                        value
                    )
                )
                progress_icon_1.invalidate()
            }
            start()
        }

        progress_step_1?.let {
            it.doOnEnd {
                progress_1?.let { view ->
                    val height = view.height
                    val width = progress_1_container.width / 2

                    ValueAnimator().apply {
                        setFloatValues(0f, width.toFloat())
                        setDuration(ANIMATION_DURATION.toLong())
                        addUpdateListener { animation ->
                            val value = animation.animatedValue as Float
                            view.layoutParams = LayoutParams(value.toInt(), height)
//                            Log.e("MES", "width: $value")
                            view.invalidate()
                            invalidate()
                        }
                        start()
                    }

                }
            }
            it.setProgress(1f, 1f, true)
        }
    }

    fun onSecondStepComplete() {
        progress_1?.let { view ->
            val height = view.height
            val startWidth = progress_1_container.width / 2
            val endWidth = progress_1_container.width

            ValueAnimator().apply {
                setIntValues(startWidth, endWidth)
                duration = ANIMATION_DURATION.toLong()

                addUpdateListener { animation ->
                    val value = animation.animatedValue as Int
                    view.layoutParams = LayoutParams(value, height)
                    view.invalidate()
                    invalidate()
                }

                doOnEnd {
                    ValueAnimator.ofFloat(0f, 100f).apply {
                        duration = ANIMATION_DURATION.toLong() * 3
                        addUpdateListener { anim ->
                            val value = anim.animatedValue as Float
                            progress_icon_2.setImageBitmap(
                                context.createGradientBitmap(
                                    R.drawable.ic_id_info,
                                    endColor,
                                    startColor,
                                    value
                                )
                            )
                            progress_icon_2.invalidate()
                        }
                        start()
                    }
                    progress_step_2?.let {
                        it.doOnEnd {
                            progress_2?.let { view ->
                                val height = view.height
                                val width = progress_1_container.width / 2

                                ValueAnimator().apply {
                                    setIntValues(0, width)
                                    duration = ANIMATION_DURATION.toLong()
                                    addUpdateListener { animation ->
                                        val value = animation.animatedValue as Int
                                        view.layoutParams = LayoutParams(value, height)
                                        view.invalidate()
                                        invalidate()
                                    }
                                    start()
                                }

                            }
                        }
                        it.setProgress(1f, 1f, true)
                    }
                }
                start()
            }

        }
        progress_icon_1?.let {

            val gradientDrawable = GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                intArrayOf(startColor, endColor)
            )
            gradientDrawable.cornerRadius = 0f
        }
    }

    fun onThirdStepComplete() {
        progress_2?.let { view ->
            val height = view.height
            val startWidth = progress_2_container.width / 2
            val endWidth = progress_2_container.width

            ValueAnimator().apply {
                setIntValues(startWidth, endWidth)
                duration = ANIMATION_DURATION.toLong()

                addUpdateListener { animation ->
                    val value = animation.animatedValue as Int
                    view.layoutParams = LayoutParams(value, height)
                    view.invalidate()
                    invalidate()
                }

                doOnEnd {
                    ValueAnimator.ofFloat(0f, 100f).apply {
                        duration = ANIMATION_DURATION.toLong() * 3
                        addUpdateListener { anim ->
                            val value = anim.animatedValue as Float
                            progress_icon_3.setImageBitmap(
                                context.createGradientBitmap(
                                    R.drawable.ic_info,
                                    endColor,
                                    startColor,
                                    value
                                )
                            )
                            progress_icon_3.invalidate()
                        }
                        start()
                    }

                    progress_step_3?.let {
                        it.doOnEnd {
                            progress_3?.let { view ->
                                val height = view.height
                                val width = progress_2_container.width / 2

                                ValueAnimator().apply {
                                    setIntValues(0, width)
                                    duration = ANIMATION_DURATION.toLong()
                                    addUpdateListener { animation ->
                                        val value = animation.animatedValue as Int
                                        view.layoutParams = LayoutParams(value, height)
                                        view.invalidate()
                                        invalidate()
                                    }
                                    start()
                                }

                            }
                        }
                        it.setProgress(1f, 1f, true)
                    }
                }
                start()
            }

        }
    }

    fun onFourthStepComplete() {
        progress_3?.let { view ->
            val height = view.height
            val startWidth = progress_3_container.width / 2
            val endWidth = progress_3_container.width

            ValueAnimator().apply {
                setIntValues(startWidth, endWidth)
                duration = ANIMATION_DURATION.toLong()

                addUpdateListener { animation ->
                    val value = animation.animatedValue as Int
                    view.layoutParams = LayoutParams(value, height)
                    view.invalidate()
                    invalidate()
                }

                doOnEnd {
                    ValueAnimator.ofFloat(0f, 100f).apply {
                        duration = ANIMATION_DURATION.toLong() * 3
                        addUpdateListener { anim ->
                            val value = anim.animatedValue as Float
                            progress_icon_4.setImageBitmap(
                                context.createGradientBitmap(
                                    R.drawable.ic_clock,
                                    endColor,
                                    startColor,
                                    value
                                )
                            )
                            progress_icon_4.invalidate()
                        }
                        start()
                    }
                    progress_step_4?.let {
                        it.setProgress(1f, 1f, true)
                    }
                }
                start()
            }
        }
    }

    fun onFourthStepReset() {
        ValueAnimator.ofFloat(100f, 0f).apply {
            duration = ANIMATION_DURATION.toLong() * 3
            addUpdateListener { anim ->
                val value = anim.animatedValue as Float
                progress_icon_4.setImageBitmap(
                    context.createGradientBitmap(
                        R.drawable.ic_clock,
                        endColor,
                        startColor,
                        value
                    )
                )

                progress_step_4?.let {
                    it.setProgress(value, 100f, false)
                    it.invalidate()
                    invalidate()
                }
                progress_icon_4.invalidate()
            }

            doOnEnd {
                progress_3?.let { view ->
                    val height = view.height
                    val startWidth = progress_3_container.width
                    val endWidth = progress_3_container.width / 2

                    ValueAnimator().apply {
                        setIntValues(startWidth, endWidth)
                        duration = ANIMATION_DURATION.toLong()

                        addUpdateListener { animation ->
                            val value = animation.animatedValue as Int
                            view.layoutParams = LayoutParams(value, height)
                            view.invalidate()
                            invalidate()
                        }

                        start()
                    }
                }
            }
            start()
        }
    }

    fun onThirdStepReset() {
        progress_3?.let { view ->
            val height = view.height
            val width = progress_2_container.width / 2

            ValueAnimator().apply {
                setIntValues(width, 0)
                duration = ANIMATION_DURATION.toLong()
                addUpdateListener { animation ->
                    val value = animation.animatedValue as Int
                    view.layoutParams = LayoutParams(value, height)
                    view.invalidate()
                    invalidate()
                }
                doOnEnd {
                    ValueAnimator.ofFloat(100f, 0f).apply {
                        duration = ANIMATION_DURATION.toLong() * 3
                        addUpdateListener { anim ->
                            val value = anim.animatedValue as Float
                            progress_icon_3.setImageBitmap(
                                context.createGradientBitmap(
                                    R.drawable.ic_info,
                                    endColor,
                                    startColor,
                                    value
                                )
                            )

                            progress_step_3?.let {
                                it.setProgress(value, 100f, false)
                            }
                            progress_icon_3.invalidate()
                        }

                        doOnEnd {
                            progress_2?.let { view ->
                                val height = view.height
                                val startWidth = progress_2_container.width
                                val endWidth = progress_2_container.width / 2

                                ValueAnimator().apply {
                                    setIntValues(startWidth, endWidth)
                                    duration = ANIMATION_DURATION.toLong()

                                    addUpdateListener { animation ->
                                        val value = animation.animatedValue as Int
                                        view.layoutParams = LayoutParams(value, height)
                                        view.invalidate()
                                        invalidate()
                                    }

                                    start()
                                }
                            }
                        }
                        start()
                    }
                }
                start()
            }

        }
    }

    fun onSecondStepReset() {
        progress_2?.let { view ->
            val height = view.height
            val width = progress_1_container.width / 2

            ValueAnimator().apply {
                setIntValues(width, 0)
                duration = ANIMATION_DURATION.toLong()
                addUpdateListener { animation ->
                    val value = animation.animatedValue as Int
                    view.layoutParams = LayoutParams(value, height)
                    view.invalidate()
                    invalidate()
                }
                doOnEnd {
                    ValueAnimator.ofFloat(100f, 0f).apply {
                        duration = ANIMATION_DURATION.toLong() * 3
                        addUpdateListener { anim ->
                            val value = anim.animatedValue as Float
                            progress_icon_2.setImageBitmap(
                                context.createGradientBitmap(
                                    R.drawable.ic_id_info,
                                    endColor,
                                    startColor,
                                    value
                                )
                            )

                            progress_step_2?.let {
                                it.setProgress(value, 100f, false)
                            }
                            progress_icon_2.invalidate()
                        }

                        doOnEnd {
                            progress_1?.let { view ->
                                val height = view.height
                                val startWidth = progress_1_container.width
                                val endWidth = progress_1_container.width / 2

                                ValueAnimator().apply {
                                    setIntValues(startWidth, endWidth)
                                    duration = ANIMATION_DURATION.toLong()

                                    addUpdateListener { animation ->
                                        val value = animation.animatedValue as Int
                                        view.layoutParams = LayoutParams(value, height)
                                        view.invalidate()
                                        invalidate()
                                    }

                                    start()
                                }
                            }
                        }
                        start()
                    }
                }
                start()
            }

        }
    }

//    private fun setupAttributes(attrs: AttributeSet?) {
//        val typedArray = context.theme.obtainStyledAttributes(
//            attrs, R.styleable.StepView,
//            0, 0
//        )
//
//        typedArray.recycle()
//    }

    companion object {
        private val ANIMATION_DURATION = 250
    }
}