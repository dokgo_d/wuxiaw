package com.dokgo.wuxiaworld.ui.home.detail

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.dokgo.domain.entity.NovelDetails
import com.dokgo.usecases.GetNovelDetailsUseCase
import com.dokgo.wuxiaworld.ui.home.HomeFragment
import com.dokgo.wuxiaworld.viewmodel.BaseViewModel
import javax.inject.Inject

class DetailViewModel @Inject constructor(
    private val getNovelDetailsUseCase: GetNovelDetailsUseCase
) : BaseViewModel() {

    val error by lazy { MutableLiveData<Throwable>() }
    val details by lazy { MutableLiveData<NovelDetails>() }

    fun getDetails(slug: String) = getNovelDetailsUseCase.execute(slug)
        .doOnSubscribe { Log.e("DetailViewModel", "doOnSubscribe") }
        .doOnError { error.postValue(it) }
        .doFinally { Log.e("DetailViewModel", "doFinally") }
        .subscribe({
            Log.e("DetailViewModel", "response: $it")
            details.postValue(it)
        }, {
            Log.e("DetailViewModel", "onError: ${it.localizedMessage}")
            it.printStackTrace()
        }).let { addDisposable(it) }
}