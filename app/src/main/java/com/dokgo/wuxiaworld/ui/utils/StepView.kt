package com.dokgo.wuxiaworld.ui.utils

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.dokgo.wuxiaworld.R

class StepView(context: Context, attrs: AttributeSet?) : View(context, attrs) {
    private var radius = 60f
    private var borderWidth = 1.dpToPx()
    private var strokeColor = Color.RED
    private var strokeColorCompleted = Color.BLUE
    private var padding = 0f
    private var margin = 10f

    override fun onDraw(canvas: Canvas?) {
//        drawLine(canvas)
        super.onDraw(canvas)
    }

    init {
//        setupAttributes(attrs)
    }

//    private fun drawCircles(canvas: Canvas?) = canvas?.apply {
//        val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
//            color = Color.RED
//            style = Paint.Style.STROKE
//        }
//        val count = 4
//        val distance = (width - padding * 2) / count
//
//        radius = distance / 2f - margin
//
//        Log.e("StepView", "radius: $radius, distance: $distance")
//
//        val size = radius * 2
//        val y = (height / 2).toFloat()
//        val offset = radius + padding
//
//        for (i: Int in 0 until count) {
//            drawCircle(offset + (i * distance), y, radius, paint)
////            drawCircle((i * distance + padding * 2), (height / 2).toFloat(), 10f, paint)
//        }
//    }

//    private fun setupAttributes(attrs: AttributeSet?) {
//        val typedArray = context.theme.obtainStyledAttributes(
//            attrs, R.styleable.StepView,
//            0, 0
//        )
//
//        radius = typedArray.getDimension(R.styleable.StepView_android_radius, 0f)
//        padding = typedArray.getDimension(R.styleable.StepView_android_padding, 0f)
//        strokeColor = typedArray.getColor(R.styleable.StepView_strokeColor, Color.RED)
//        strokeColorCompleted =
//            typedArray.getColor(R.styleable.StepView_strokeColorCompleted, Color.BLUE)
//
//        typedArray.recycle()
//    }
}