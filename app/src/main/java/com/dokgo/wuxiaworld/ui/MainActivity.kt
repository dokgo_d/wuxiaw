package com.dokgo.wuxiaworld.ui

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.dokgo.wuxiaworld.R
import com.dokgo.wuxiaworld.ui.base.BaseActivity
import com.dokgo.wuxiaworld.ui.utils.ScreenUtil
import com.dokgo.wuxiaworld.viewmodel.ViewModelFactory
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var screen: ScreenUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navigation_host_fragment) as NavHostFragment?

        bottom_navigation_view.itemIconTintList = null
        NavigationUI.setupWithNavController(
            bottom_navigation_view, navHostFragment!!.navController
        )
    }

    fun hideBottomNav() {
        bottom_navigation_view.visibility = View.GONE
    }

    fun showBottomNav() {
        bottom_navigation_view.visibility = View.VISIBLE
    }
}
