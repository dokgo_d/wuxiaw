package com.dokgo.wuxiaworld.ui.base

import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonSyntaxException
import retrofit2.HttpException
import java.net.HttpURLConnection

abstract class BaseActivity : AppCompatActivity() {

    fun handleApiError(error: Throwable) {
        if (error is HttpException) {
            val message = when (error.code()) {
                HttpURLConnection.HTTP_NOT_FOUND -> "NOT FOUND"
                else -> error.message()
            }

            showError(message)
        } else if (error is JsonSyntaxException){
            showError(error.localizedMessage)
        }
    }

    fun showError(message: String, duration: Int = Snackbar.LENGTH_SHORT) {
        Snackbar.make(findViewById(android.R.id.content), message, duration).show()
    }
}