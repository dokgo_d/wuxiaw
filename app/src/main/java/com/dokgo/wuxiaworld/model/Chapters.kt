package com.dokgo.wuxiaworld.model

import com.dokgo.domain.entity.Chapter as DomainChapter
import com.dokgo.domain.entity.Chapters as DomainChapters

data class Chapters(
    val id: Int,
    val fromChapterNumber: Int,
    val novelId: Int,
    val order: Int,
    val title: String,
    val toChapterNumber: Int,
    val chapters: List<Chapter>
)

data class Chapter(
    val id: Int,
    val number: Int,
    val whenToPublish: Long,
    val name: String,
    val slug: String,
    val advanceChapter: Boolean,
    val spoilerTitle: Boolean,
    val isTeaser: Boolean
)

fun DomainChapter.toModel() = Chapter(
    id = id,
    number = number?.toInt() ?: 0,
    whenToPublish = whenToPublish ?: 0,
    name = name ?: "",
    slug = slug ?: "",
    advanceChapter = advanceChapter ?: false,
    spoilerTitle = spoilerTitle ?: false,
    isTeaser = isTeaser ?: false
)

fun DomainChapters.toModel() = Chapters(
    id = id,
    fromChapterNumber = fromChapterNumber?.toInt() ?: 0,
    novelId = novelId ?: 0,
    order = order ?: 0,
    title = title ?: "",
    toChapterNumber = toChapterNumber?.toInt() ?: 0,
    chapters = chapters?.map { it.toModel() } ?: emptyList()
)