package com.dokgo.wuxiaworld.model

import com.dokgo.domain.entity.Novel as DomainNovel

data class Novel(
    val id: Int,
    val name: String,
    val active: Boolean,
    val abbreviation: String,
    val slug: String,
    val language: String,
    val languageAbbreviation: String,
    val visible: Boolean,
    val description: String,
    val synopsis: String,
    val coverUrl: String,
    val translatorId: String,
    val translatorUserName: String,
    val authorName: String,
    val siteCreditsEnabled: Boolean,
    val teaserMessage: String,
    val isFree: Boolean,
    val karmaActive: Boolean,
    val novelHasSponsorPlans: Boolean,
    val userHasEbook: Boolean,
    val userHasNovelUnlocked: Boolean,
    val chapterGroups: String,
    val tags: List<String>,
    val genres: List<String>,
    val sponsorPlans: String,
    val latestAnnouncement: String,
    val ebooks: List<String>
)

object Mock {
    fun novel(id: Int) = Novel(
        id = id,
        name = "",
        active = false,
        abbreviation = "",
        slug = "",
        language = "",
        languageAbbreviation = "",
        visible = false,
        description = "",
        synopsis = "",
        coverUrl = "",
        translatorId = "",
        translatorUserName = "",
        authorName = "",
        siteCreditsEnabled = false,
        teaserMessage = "",
        isFree = false,
        karmaActive = false,
        novelHasSponsorPlans = false,
        userHasEbook = false,
        userHasNovelUnlocked = false,
        chapterGroups = "",
        tags = emptyList(),
        genres = emptyList(),
        sponsorPlans = "",
        latestAnnouncement = "",
        ebooks = emptyList()
    )
}

fun DomainNovel.toModel(): Novel = Novel(
    id = id,
    name = name ?: "",
    active = active ?: false,
    abbreviation = abbreviation ?: "",
    slug = slug ?: "",
    language = language ?: "",
    languageAbbreviation = languageAbbreviation ?: "",
    visible = visible ?: false,
    description = description ?: "",
    synopsis = synopsis ?: "",
    coverUrl = coverUrl ?: "",
    translatorId = translatorId ?: "",
    translatorUserName = translatorUserName ?: "",
    authorName = authorName ?: "",
    siteCreditsEnabled = siteCreditsEnabled ?: false,
    teaserMessage = teaserMessage ?: "",
    isFree = isFree ?: false,
    karmaActive = karmaActive ?: false,
    novelHasSponsorPlans = novelHasSponsorPlans ?: false,
    userHasEbook = userHasEbook ?: false,
    userHasNovelUnlocked = userHasNovelUnlocked ?: false,
    chapterGroups = chapterGroups ?: "",
    tags = tags ?: emptyList(),
    genres = genres ?: emptyList(),
    sponsorPlans = sponsorPlans ?: "",
    latestAnnouncement = latestAnnouncement ?: "",
    ebooks = ebooks ?: emptyList()
)

fun Novel.isEmpty() = name == "" && slug == ""

fun Novel.isNotEmpty() = isEmpty().not()