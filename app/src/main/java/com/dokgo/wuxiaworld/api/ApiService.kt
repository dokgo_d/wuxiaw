package com.dokgo.wuxiaworld.api

import com.dokgo.wuxiaworld.api.response.ChaptersResponse
import com.dokgo.wuxiaworld.api.response.NovelDetailsResponse
import com.dokgo.wuxiaworld.api.response.NovelsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("novels")
    fun getNovels(): Single<NovelsResponse>

    @GET("novels/chapters/{slug}")
    fun getChapters(@Path("slug") slug: String): Single<ChaptersResponse>

    @GET("novels/details/{slug}")
    fun getNovelDetails(@Path("slug") slug: String): Single<NovelDetailsResponse>



    companion object {
        const val BASE_URL = "https://www.wuxiaworld.com/api/"
    }
}