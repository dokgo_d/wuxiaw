package com.dokgo.wuxiaworld.api.response

import com.dokgo.domain.entity.Chapters
import com.dokgo.domain.entity.Novel
import com.dokgo.domain.entity.NovelDetails

typealias NovelsResponse = BaseResponse<List<Novel>>
typealias ChaptersResponse = BaseResponse<List<Chapters>>
typealias NovelDetailsResponse = BaseResponse<NovelDetails>

//data class NovelsResponse(
//    val total: Int,
//    val items: List<Novel>,
//    val result: Boolean
//)