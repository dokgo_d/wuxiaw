package com.dokgo.wuxiaworld.api.response

import com.google.gson.annotations.SerializedName

open class BaseResponse<T>(
    @SerializedName("items") val dataList: T?,
    @SerializedName("item") val data: T?,
    @SerializedName("result") val result: Boolean
)