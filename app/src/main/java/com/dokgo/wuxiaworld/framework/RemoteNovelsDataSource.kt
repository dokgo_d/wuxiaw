package com.dokgo.wuxiaworld.framework

import com.dokgo.data.INovelsDataSource
import com.dokgo.domain.entity.Chapter
import com.dokgo.domain.entity.Novel
import com.dokgo.domain.entity.NovelDetails
import com.dokgo.wuxiaworld.api.ApiService
import io.reactivex.Single

class RemoteNovelsDataSource(private val apiService: ApiService) : INovelsDataSource {
    override fun getNovels(): Single<List<Novel>> =
        apiService.getNovels().map { it.dataList }

    override fun getChapters(slug: String): Single<List<Chapter>> =
        apiService.getChapters(slug).map { it.dataList?.first()?.chapters }

    override fun getNovelDetails(slug: String): Single<NovelDetails> =
        apiService.getNovelDetails(slug).map { it.data }

}