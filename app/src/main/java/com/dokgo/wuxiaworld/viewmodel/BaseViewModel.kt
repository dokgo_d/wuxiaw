package com.dokgo.wuxiaworld.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dokgo.wuxiaworld.ui.home.HomeFragment
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel() {
    private val disposables = CompositeDisposable()
    open val status by lazy { MutableLiveData<Status>() }

    fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

    override fun onCleared() {
        if (disposables.isDisposed.not()) disposables.dispose()
        super.onCleared()
    }
}

enum class Status { Loading, Loaded }