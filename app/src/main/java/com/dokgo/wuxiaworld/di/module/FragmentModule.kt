package com.dokgo.wuxiaworld.di.module

import com.dokgo.wuxiaworld.ui.home.HomeFragment
import com.dokgo.wuxiaworld.ui.home.detail.DetailFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeDetailFragment(): DetailFragment
}