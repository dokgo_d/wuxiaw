package com.dokgo.wuxiaworld.di.component

import com.dokgo.wuxiaworld.App
import com.dokgo.wuxiaworld.di.module.NetworkModule
import com.dokgo.wuxiaworld.di.module.ActivityModule
import com.dokgo.wuxiaworld.di.module.AppModule
import com.dokgo.wuxiaworld.di.module.FragmentModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Component(
    modules = [
        NetworkModule::class, AppModule::class, ActivityModule::class,
        AndroidInjectionModule::class, FragmentModule::class
    ]
)
@Singleton
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }

    fun inject(app: App)
}