package com.dokgo.wuxiaworld.di.module

import com.dokgo.data.INovelsDataSource
import com.dokgo.data.NovelsRepository
import com.dokgo.usecases.GetChaptersUseCase
import com.dokgo.usecases.GetNovelDetailsUseCase
import com.dokgo.usecases.GetNovelsUseCase
import com.dokgo.wuxiaworld.BuildConfig
import com.dokgo.wuxiaworld.api.ApiService
import com.dokgo.wuxiaworld.api.ApiService.Companion.BASE_URL
import com.dokgo.wuxiaworld.framework.RemoteNovelsDataSource
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule {

    @Provides
    fun provideOkHttp(): OkHttpClient {
        val client = OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(logging)
        }

        return client.build()
    }

    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(BASE_URL)
            .build()
    }

    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Provides
    fun provideNovelsDataSource(apiService: ApiService): INovelsDataSource =
        RemoteNovelsDataSource(apiService)

    @Provides
    fun provideNovelsRepository(dataSource: INovelsDataSource): NovelsRepository =
        NovelsRepository(dataSource)

    @Provides
    fun provideGetNovelsUseCase(repository: NovelsRepository): GetNovelsUseCase =
        GetNovelsUseCase(repository)

    @Provides
    fun provideGetChaptersUseCase(repository: NovelsRepository): GetChaptersUseCase =
        GetChaptersUseCase(repository)

    @Provides
    fun provideGetNovelDetailsUseCase(repository: NovelsRepository): GetNovelDetailsUseCase =
        GetNovelDetailsUseCase(repository)
}