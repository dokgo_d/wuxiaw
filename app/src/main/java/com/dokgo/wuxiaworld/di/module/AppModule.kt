package com.dokgo.wuxiaworld.di.module

import android.content.Context
import com.dokgo.wuxiaworld.App
import com.dokgo.wuxiaworld.ui.utils.ScreenUtil
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {
    //
    @Provides
    @Singleton
    fun provideApplication(app: App): Context = app

    @Provides
    @Singleton
    fun provideScreenUtils(context: Context) = ScreenUtil(context)
}