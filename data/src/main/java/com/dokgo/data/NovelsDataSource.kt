package com.dokgo.data

import com.dokgo.domain.entity.Chapter
import com.dokgo.domain.entity.Novel
import com.dokgo.domain.entity.NovelDetails
import io.reactivex.Single

interface INovelsDataSource {
    fun getNovels(): Single<List<Novel>>

    fun getChapters(slug: String): Single<List<Chapter>>

    fun getNovelDetails(slug: String): Single<NovelDetails>
}