package com.dokgo.data

class NovelsRepository(private val dataSource: INovelsDataSource) {
    fun getNovels() = dataSource.getNovels()

    fun getChapters(slug: String) = dataSource.getChapters(slug)

    fun getDetails(slug: String) = dataSource.getNovelDetails(slug)
}