package com.dokgo.usecases

import com.dokgo.data.NovelsRepository
import io.reactivex.Single
import com.dokgo.domain.entity.Novel
import io.reactivex.schedulers.Schedulers

class GetNovelsUseCase(private val repository: NovelsRepository) {
    fun execute(): Single<List<Novel>> =
        repository.getNovels()
            .subscribeOn(Schedulers.io())

}