package com.dokgo.usecases

import com.dokgo.data.NovelsRepository
import com.dokgo.domain.entity.Chapter
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetChaptersUseCase(private val repository: NovelsRepository) {

    fun execute(slug: String): Single<List<Chapter>> =
        repository.getChapters(slug)
            .subscribeOn(Schedulers.io())

}