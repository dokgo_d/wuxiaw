package com.dokgo.usecases

import com.dokgo.data.NovelsRepository
import com.dokgo.domain.entity.NovelDetails
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class GetNovelDetailsUseCase(private val repository: NovelsRepository) {

    fun execute(slug: String): Single<NovelDetails> =
        repository.getDetails(slug)
            .subscribeOn(Schedulers.io())

}