package com.dokgo.domain.entity

data class Chapters (
	val id : Int,
	val fromChapterNumber : Double? = null,
	val novelId : Int? = null,
	val order : Int? = null,
	val title : String? = null,
	val toChapterNumber : Double? = null,
	val chapters : List<Chapter>? = null
)

data class Chapter (
	val id : Int,
	val number : Double? = null,
	val whenToPublish : Long? = null,
	val name : String? = null,
	val slug : String? = null,
	val advanceChapter : Boolean? = null,
	val spoilerTitle : Boolean? = null,
	val isTeaser : Boolean? = null
)