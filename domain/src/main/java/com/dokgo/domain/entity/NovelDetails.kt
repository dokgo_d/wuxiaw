package com.dokgo.domain.entity

data class NovelDetails(
    val id: Int,
    val name: String? = null,
    val active: Boolean? = null,
    val abbreviation: String? = null,
    val slug: String? = null,
    val language: String? = null,
    val languageAbbreviation: String? = null,
    val visible: Boolean? = null,
    val description: String? = null,
    val synopsis: String? = null,
    val coverUrl: String? = null,
    val translatorId: String? = null,
    val translatorName: String? = null,
    val translatorUserName: String? = null,
    val authorName: String? = null,
    val siteCreditsEnabled: Boolean? = null,
    val teaserMessage: String? = null,
    val isFree: Boolean? = null,
    val karmaActive: Boolean? = null,
    val novelHasSponsorPlans: Boolean? = null,
    val userHasEbook: Boolean? = null,
    val userHasNovelUnlocked: Boolean? = null,
    val reviewScore: Double? = null,
    val chapterGroups: String? = null,
    val tags: List<Tags>? = null,
    val genres: List<Genres>? = null,
    val sponsorPlans: List<String>? = null,
    val latestAnnouncement: String? = null,
    val ebooks: List<EBook>? = null
)

data class Genres(
    val id: Int,
    val name: String? = null
)

data class Tags(
    val id: Int,
    val name: String? = null,
    val restricted: Boolean? = null
)

data class EBook(
    val id: Int,
    val name: String? = null,
    val coverUrl: String? = null,
    val order: Int? = null,
    val description: String? = null,
    val dateCreated: Double? = null,
    val novelId: Int? = null,
    val product: Product? = null
)

data class Product(
    val id: Int,
    val name: String? = null,
    val price: Int? = null,
    val discount: String? = null,
    val quantity: Int? = null,
    val description: String? = null,
    val enabled: Boolean? = null,
    val type: Int? = null,
    val isSubscription: Boolean? = null,
    val dateCreated: Double? = null,
    val stripeProductId: String? = null,
    val appleProductId: String? = null,
    val googleProductId: String? = null,
    val order: String? = null
)